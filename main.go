/*
 * Copyright © 2018  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package main

import (
	"fmt"
	"os"
	"os/user"
	"path"

	"github.com/urfave/cli/v2"

	"sgitch/actions"
	"sgitch/actions/profiles"
	"sgitch/constants"
)

var (
	app *cli.App
)

/**
 * Function prints error and exits if the error is set.
 */
func panicOnError(description string, err error, exitCode int) {
	if err != nil {
		fmt.Print(description + ": ")
		os.Exit(exitCode)
	}
}

/**
 * Main function, start of program.
 */
func main() {
	usr, userCurrentError := user.Current()
	panicOnError("user.Current", userCurrentError, 1)

	app = cli.NewApp()
	app.Name = "Sgitch"
	app.Usage = "Switch between git \"profiles\"."
	app.Version = "1.2.0"
	app.Copyright = "© 2021 Kiririn.io"
	app.EnableBashCompletion = true
	app.CommandNotFound = func(context *cli.Context, command string) {
		fmt.Printf(
			"Unknown command \"%s\"."+
				"\nSee `--help` for available commands.\n",
			command)
		os.Exit(2)
	}
	app.Authors = []*cli.Author{
		{
			Name:  "Tim van Leuverden",
			Email: "TvanLeuverden@Gmail.com",
		},
	}

	app.Flags = []cli.Flag{
		&cli.PathFlag{
			Name:      constants.ConfigPath,
			Aliases:   []string{"c"},
			Value:     path.Join(usr.HomeDir, ".config", "sgitch.yml"),
			Usage:     "use the `FILE` as the config file",
			TakesFile: true,
		},
	}
	app.Commands = []*cli.Command{
		{
			Name:      "set",
			Aliases:   []string{"s"},
			Usage:     "set the current profile",
			ArgsUsage: "[NAME]",
			Action:    actions.Set,
		},
		{
			Name:    "init",
			Aliases: []string{"i"},
			Usage:   "initialize a new configuration",
			Flags: []cli.Flag{
				&cli.BoolFlag{
					Name:  "force",
					Usage: "override the file if it already exists",
				},
			},
			Action: actions.Init,
		},
		{
			Name:    "profiles",
			Aliases: []string{"profile", "p"},
			Usage:   "manage profiles",
			Action:  profiles.List,
			Subcommands: []*cli.Command{
				{
					Name:    "list",
					Aliases: []string{"l"},
					Usage:   "list profiles",
					Action:  profiles.List,
				},
				{
					Name:    "add",
					Aliases: []string{"a"},
					Usage:   "add a profile",
					Action:  profiles.Add,
				},
				{
					Name:      "edit",
					Aliases:   []string{"e"},
					Usage:     "edit a profile",
					ArgsUsage: "[NAME]",
					Action:    profiles.Edit,
				},
				{
					Name:    "remove",
					Aliases: []string{"r", "rm"},
					Usage:   "remove a profile",
					Action:  profiles.Remove,
				},
			},
		},
	}
	err := app.Run(os.Args)
	panicOnError("App run failed", err, 2)
	os.Exit(0)
}
