/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package actions

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/urfave/cli/v2"
	"gopkg.in/ini.v1"

	"sgitch/constants"
	"sgitch/utils"
)

func Set(context *cli.Context) error {
	if context.Args().Len() != 1 {
		return cli.Exit(
			"Requires 1 argument, `NAME`.\n"+
				"See `set --help` for usage.",
			11)
	}
	configPath := context.String(constants.ConfigPath)
	profileName := context.Args().Get(0)
	profileName = strings.ToLower(profileName)

	// Read config from file
	config, configError := utils.ReadConfig(configPath)
	if configError != nil {
		return cli.Exit(configError, 221)
	}

	// Finding the profile in the config
	profile, isFound := config.Profiles[profileName]
	if !isFound {
		return cli.Exit(
			"Profile no found in config.\n"+
				"Create new profile with `profile add`.",
			15)
	}

	basePath := "."
	gitConfigPath := ""

	for true {
		// Check if the git config file exists
		gitConfigPath = path.Join(basePath, ".git", "config")
		if fileExists(gitConfigPath) {
			break
		}

		// If absolute base path can't be resolved or is the root, exit
		if abs, err := filepath.Abs(basePath); err != nil || abs == path.Join("/") {
			return cli.Exit(
				"Not in a git repository.",
				16)
		}

		// Go up one dir
		basePath = path.Join(basePath, "..")
	}

	// Load Git config file
	gitConfig, err := ini.ShadowLoad(gitConfigPath)
	if err != nil {
		return cli.Exit(
			fmt.Sprintf("Could not open git config `%s`.", gitConfigPath),
			17)
	}

	// Set the values in the config file.
	gitConfigUser := gitConfig.Section("user")
	gitConfigUser.Key("name").SetValue(profile.Name)
	gitConfigUser.Key("email").SetValue(profile.Email)
	gitConfigUser.Key("signingkey").SetValue(profile.Signing.Key)
	gitConfig.Section("commit").Key("gpgSign").SetValue(strconv.FormatBool(profile.Signing.IsEnabled))

	if err = gitConfig.SaveTo(gitConfigPath); err != nil {
		return cli.Exit(
			fmt.Sprintf("Could not save git config `%s`. %s", gitConfigPath, err),
			17)
	}

	fmt.Println("Successfully changed profile.")
	return nil
}

func fileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
