/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package profiles

import (
	"fmt"

	"github.com/urfave/cli/v2"

	"sgitch/constants"
	"sgitch/utils"
)

func List(context *cli.Context) error {
	// Read config file
	configPath := context.String(constants.ConfigPath)
	config, err := utils.ReadConfig(configPath)
	if err != nil {
		return cli.Exit(err, 221)
	}

	// Loop through profiles, and print each one
	fmt.Printf(
		"Profiles found in \"%s\":\n",
		configPath)
	for key, value := range config.Profiles {
		fmt.Println("\n" + value.ToString(&key))
	}

	return nil
}
