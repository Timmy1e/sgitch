/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package profiles

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"

	"sgitch/constants"
	"sgitch/models"
	"sgitch/utils"
)

func Add(context *cli.Context) error {
	profile := models.Profile{
		Signing: models.Signing{},
	}

	// Read config file
	configPath := context.String(constants.ConfigPath)
	config, err := utils.ReadConfig(configPath)
	if err != nil {
		return cli.Exit(err, 231)
	}

	// Ask for profile name, and transform
	profileName := utils.AskString("Profile name (nickname)")
	profileName = strings.ToLower(profileName)

	// Check if profile exists already
	if _, ok := config.Profiles[profileName]; ok {
		return cli.Exit(
			fmt.Sprintf(
				"Profile name is already used. (Case INSENSITIVE)\nEdit this profile with `profiles edit %s`.",
				profileName),
			232)
	}

	// Ask for data, and add to profile
	profile.Name = utils.AskString("Name")
	profile.Email = utils.AskString("Email")
	profile.Signing.IsEnabled = utils.AskBool("Signing enabled")
	if profile.Signing.IsEnabled {
		profile.Signing.Key = utils.AskString("Signing key")
	}

	// Add profile to config
	config.Profiles[profileName] = profile

	// Write config back to file
	err = utils.WriteConfig(configPath, config)
	if err != nil {
		return cli.Exit(err, 233)
	}

	fmt.Println(fmt.Sprintf(
		"Added profile \"%s\" to \"%s\".",
		profileName, configPath))
	return nil
}
