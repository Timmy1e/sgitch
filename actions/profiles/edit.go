/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package profiles

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"

	"sgitch/constants"
	"sgitch/utils"
)

func Edit(context *cli.Context) error {
	if context.Args().Len() != 1 {
		return cli.Exit(
			"Requires 1 argument, `NAME`.\nSee `profiles edit --help` for usage.",
			241)
	}
	configPath := context.String(constants.ConfigPath)
	profileName := context.Args().Get(0)
	profileName = strings.ToLower(profileName)

	// Read the config.
	config, err := utils.ReadConfig(configPath)
	if err != nil {
		return cli.Exit(err, 242)
	}

	// Get profile from config.
	profile, isFound := config.Profiles[profileName]
	if !isFound {
		return cli.Exit(
			"Profile no found in config.\nCreate new profile with `profiles add`.",
			243)
	}

	// Ask for data, and update.
	profile.Name = utils.AskStringOptional("Name", profile.Name)
	profile.Email = utils.AskStringOptional("Email", profile.Email)
	profile.Signing.IsEnabled = utils.AskBool("Signing enabled")
	if profile.Signing.IsEnabled {
		profile.Signing.Key = utils.AskStringOptional("Signing key", profile.Signing.Key)
	}

	// Override the existing profile with the changed one.
	// TODO: Allow users to change profileName
	config.Profiles[profileName] = profile

	// Write config back to file.
	err = utils.WriteConfig(configPath, config)
	if err != nil {
		return cli.Exit(err, 244)
	}

	fmt.Printf(
		"Updated profile \"%s\" in \"%s\".\n",
		profileName, configPath)
	return nil
}
