/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package profiles

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"

	"sgitch/constants"
	"sgitch/utils"
)

func Remove(context *cli.Context) error {
	if context.Args().Len() != 1 {
		return cli.Exit(
			"Requires 1 argument, `NAME`.\nSee `profiles remove --help` for usage.",
			251)
	}
	configPath := context.String(constants.ConfigPath)
	profileName := context.Args().Get(0)
	profileName = strings.ToLower(profileName)

	// Read the config
	config, err := utils.ReadConfig(configPath)
	if err != nil {
		return cli.Exit(err, 252)
	}

	// Get profile from config
	profile, isFound := config.Profiles[profileName]
	if !isFound {
		return cli.Exit(
			"Profile no found in config.\nCreate new profile with `profiles add`.",
			253)
	}

	// Ask user if they want to delete the profile
	fmt.Println(profile.ToString(&profileName))
	if !utils.AskBool("Are you sure you wish to delete this profile") {
		return cli.Exit("The profile was NOT removed.", 0)
	}

	// Remove profile from profile dict
	delete(config.Profiles, profileName)

	// Write config back to file
	err = utils.WriteConfig(configPath, config)
	if err != nil {
		return cli.Exit(err, 254)
	}

	fmt.Printf(
		"Removed profile \"%s\" from \"%s\".\n",
		profileName, configPath)
	return nil
}
