/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package actions

import (
	"fmt"
	"os"

	"github.com/urfave/cli/v2"

	"sgitch/constants"
	"sgitch/models"
	"sgitch/utils"
)

func Init(context *cli.Context) error {
	// Check if file exists, if so, exit
	configPath := context.String(constants.ConfigPath)
	if _, err := os.Stat(configPath); err == nil && !context.Bool("force") {
		return cli.Exit(
			"Config file already exists.\n"+
				"Use `init --force` to override the file.",
			211)
	}

	// Create empty config file
	if err := utils.WriteConfig(configPath, models.Config{}); err != nil {
		return cli.Exit(err, 212)
	}

	fmt.Printf(
		"Successfully created config file at \"%s\".\n",
		configPath)
	return nil
}
