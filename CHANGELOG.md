# Version 1.2.0
 - Separate code into files
 - Directly edit the Git config file, instead of using Git
 - Move `config init` to `init`
 - Change license from GPLv3 to AGPLv3+

# Version 1.1.0
 - Change spacing in changelog
 - Cleanup code
 - Move `config profiles` to `profiles`
 
# Version 1.0.0
 - Add support for `user.name`
 - Add support for `user.email`
 - Add support for `commit.gpgSign`
 - Add support for `user.signingkey`
 - Add full CLI for modifying profiles
 - Add action for setting current profile
 - Add action to initialize a config file
