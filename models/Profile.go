/*
 * Copyright © 2018  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package models

import "strconv"

/**
 * Config file profile object.
 */
type Profile struct {
	Name  string
	Email string
	Signing Signing
}

func (p Profile) ToString(profileName *string) string {
	result := ""
	if profileName != nil {
		result += *profileName + ":"
	}
	result += "\n  Name:    " + p.Name
	result += "\n  Email:   " + p.Email
	if p.Signing != (Signing{}) {
		result += "\n  Signing:"
		result += "\n    Enabled: " + strconv.FormatBool(p.Signing.IsEnabled)
		result += "\n    Key:     " + p.Signing.Key
	} else {
		result += "\n  Signing: ❌"
	}
	return result
}
