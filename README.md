# Sgitch

## Synopsis
Quickly switch between git profiles.
You can have separate profiles for: work, school, private, etc.
Currently, supports: `user.name`, `user.email`, `commit.gpgSign`, and `user.signingkey`.
The name is a concatenation between "switch" and "GIT", as "sGITch".


## Installation
Requirement versions are estimates, it's what I use and know works.

### From static binaries
Check the [tags](https://gitlab.com/Timmy1e/sgitch/tags) page.
Under every release there should be binaries for most platforms.

### Using Go Get
Requirements:
 - Go _(1.10.3+)_
 
Command:
```bash
$ go get gitlab.com/Timmy1e/sgitch
```

### Using git
Requirements:
 - Git _(2.18.0+)_
 - Go _(1.15.0+)_
 
First, clone this repo:
```bash
$ git clone git@gitlab.com:Timmy1e/sgitch.git
```
Second, tell go to build and get the dependencies.
```bash
$ go build -i .
```
Then move the build `sgitch` executable to where ever you want.


## Dependencies
 - [cli](https://github.com/urfave/cli)
 - [ini](https://gopkg.in/ini.v1)
 - [yaml](https://gopkg.in/yaml.v2)
 

## Usage
Please take note of the difference between 'name' and 'profile (name)'.
The 'name' is the name of the user, and 'profile (name)' is the nickname of the profile.

### Init
This initializes a config file for you.
```bash
$ sgitch init
```
```text
NAME:
   Sgitch init - initialize a new configuration

USAGE:
   Sgitch init [command options] [arguments...]

OPTIONS:
   --force  override the file if it already exists
```

#### Profiles
Here you can manage your profiles.
```text
COMMANDS:
     list, l        list profiles
     add, a         add a profile
     edit, e        edit a profile
     remove, r, rm  remove a profile

OPTIONS:
   --help, -h  show help
```

##### List
List profiles in config file.
```bash
$ sgitch config profiles list
```

##### Add
Add a profile to the config file.
```bash
$ sgitch config profiles add
```

##### Edit
Edit a profile in the config file.
```bash
$ sgitch config profiles edit [NAME]
```

##### Remove
Remove a profile from the config file.
```bash
$ sgitch config profiles remove [NAME]
```

### Set
Set the current profile to use.
```bash
$ sgitch set [NAME]
```

## License
GNU Affero General Public License v3.0 or later
