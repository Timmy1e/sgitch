/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/**
 * Ask user for string.
 */
func AskString(question string) string {
	reader := bufio.NewReader(os.Stdin)
	var result string

	for result == "" {
		fmt.Print(question + ": ")
		readBytes, _, _ := reader.ReadLine()
		result = string(readBytes)
	}

	return result
}

/**
 * Ask user for string (optional answer).
 */
func AskStringOptional(question string, defaultValue string) string {
	reader := bufio.NewReader(os.Stdin)
	var result string

	fmt.Print(question + " (" + defaultValue + "): ")
	readBytes, _, _ := reader.ReadLine()
	result = string(readBytes)

	if result == "" {
		result = defaultValue
	}

	return result
}

/**
 * Ask user for boolean.
 */
func AskBool(question string) bool {
	reader := bufio.NewReader(os.Stdin)
	haveResult := false
	var result bool

	for !haveResult {
		fmt.Print(question + " [y/n]: ")
		readBytes, _, _ := reader.ReadLine()
		inStr := string(readBytes)
		inStrLow := strings.ToLower(inStr)
		if inStrLow == "y" || inStrLow == "yes" || inStrLow == "t" || inStrLow == "true" {
			result = true
			haveResult = true
		} else if inStrLow == "n" || inStrLow == "no" || inStrLow == "f" || inStrLow == "false" {
			result = false
			haveResult = true
		}
	}

	return result
}
