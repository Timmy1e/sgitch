/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package utils

import (
	"io/ioutil"
	"os"

	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"

	"sgitch/models"
)

/**
 * Read config from file.
 */
func ReadConfig(configPath string) (models.Config, error) {
	/*
	 * Read config file.
	 */
	configData, readFileError := ioutil.ReadFile(configPath)
	if readFileError != nil {
		return models.Config{}, cli.Exit("Config reading error: "+readFileError.Error(), 4)
	}

	if configData == nil {
		return models.Config{}, cli.Exit(
			"Config file not found.\nCreate a new config file with `config init`.",
			13)
	}

	/*
	 * Parse config from data.
	 */
	var config models.Config
	yamlParseError := yaml.Unmarshal(configData, &config)

	if yamlParseError != nil {
		return models.Config{}, cli.Exit("Failed to parse config. Is it valid YAML?", 5)
	} else {
		return config, nil
	}
}

/**
 * Write config to file.
 */
func WriteConfig(configPath string, config models.Config) error {
	/*
	 * Convert config to YAML bytes.
	 */
	configData, marshalError := yaml.Marshal(config)
	if marshalError != nil {
		return cli.Exit("Error creating file data: "+marshalError.Error(), 6)
	}

	/*
	 * Write data to file.
	 */
	fileError := ioutil.WriteFile(configPath, configData, os.FileMode(0600))
	if fileError != nil {
		return cli.Exit("Error writing file: "+fileError.Error(), 7)
	}

	return nil
}
